# ---------------------------------------------------------------
# Docker: container enginer and associated tools
# Docker is huge compared to everything else.  So we just grab 
# the archive and let the installation tools stuff the binaries 
# into the data partition instead of in the squashfs.
# ---------------------------------------------------------------
$(DOCKER_T)-verify: 
	@if [ $(UID) -ne 0 ]; then \
		echo "You must be root to run this target."; \
		exit 1; \
	fi
	@if [ "$(OPKG_DIR)" = "" ] || [ ! -f $(OPKG_DIR)/opkg-build ]; then \
		$(MSG11) "Can't find opkg-build.  Try setting OPKG= to the directory it lives in." $(EMSG); \
		exit 1; \
	fi

# Retrieve package
$(DOCKER_T)-get: 
	@mkdir -p $(BLDDIR) $(DOCKER_ARCDIR)
	@if [ ! -f $(DOCKER_ARCDIR)/$(DOCKER_FILE) ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) "Retrieving DOCKER static binary archive" $(EMSG); \
		$(MSG) "================================================================"; \
		cd $(DOCKER_ARCDIR) && wget $(DOCKER_URL)/$(DOCKER_FILE); \
	else \
		$(MSG3) $(DOCKER) archive is cached $(EMSG); \
	fi
	@if [ ! -f $(DOCKER_ARCDIR)/$(CHECK_CONFIG_SCRIPT) ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) "Retrieving DOCKER check-config script" $(EMSG); \
		$(MSG) "================================================================"; \
		cd $(DOCKER_ARCDIR) && wget $(CHECK_CONFIG_URL)/$(CHECK_CONFIG_SCRIPT); \
	else \
		$(MSG3) $(CHECK_CONFIG_SCRIPT) is cached $(EMSG); \
	fi

# Unpack the package.
$(DOCKER_T)-unpack: $(DOCKER_T)-get
	@mkdir -p $(DOCKER_BLDDIR)
	@cd $(DOCKER_ARCDIR) && $(UNPACK_CMD)
	@touch .$(subst .,,$@)

$(DOCKER_T)-init: $(DOCKER_T)-unpack 

$(DOCKER_T): $(DOCKER_T)-init
	@$(MSG) "================================================================"
	@$(MSG2) "Packaging $(DOCKER)" $(EMSG)
	@$(MSG) "================================================================"

# Package it as an opkg 
pkg $(DOCKER_T)-pkg: $(DOCKER_T)
	@make --no-print-directory $(DOCKER_T)-verify
	@mkdir -p $(PKGDIR)/opkg/$(DOCKER_T)/CONTROL
	@mkdir -p $(PKGDIR)/opkg/$(DOCKER_T)/usr/bin
	@mkdir -p $(PKGDIR)/opkg/$(DOCKER_T)/etc/init.d
	@cp -ar $(DOCKER_BLDDIR)/docker/* $(PKGDIR)/opkg/$(DOCKER_T)/usr/bin
	@cp $(DOCKER_ARCDIR)/check-config.sh $(PKGDIR)/opkg/$(DOCKER_T)/usr/bin
	@cp $(SRCDIR)/opkg/control $(PKGDIR)/opkg/$(DOCKER_T)/CONTROL/control
	@sed -i 's/\[-PROJ-\]/$(DOCKER_T)/g' $(PKGDIR)/opkg/$(DOCKER_T)/CONTROL/control
	@sed -i 's/\[-VERSION-\]/$(BLD_VERSION)/g' $(PKGDIR)/opkg/$(DOCKER_T)/CONTROL/control
	@cp $(DIR_DOCKER)/S90docker $(PKGDIR)/opkg/$(DOCKER_T)/etc/init.d
	@cp $(SRCDIR)/opkg/postinst $(PKGDIR)/opkg/$(DOCKER_T)/CONTROL/postinst
	@cp $(SRCDIR)/opkg/debian-binary $(PKGDIR)/opkg/$(DOCKER_T)/CONTROL/debian-binary
	@chmod +x $(PKGDIR)/opkg/$(DOCKER_T)/CONTROL/postinst
	@chmod +x $(PKGDIR)/opkg/$(DOCKER_T)/etc/init.d/*
	@chmod +x $(PKGDIR)/opkg/$(DOCKER_T)/usr/bin/*
	@chown -R root.root $(PKGDIR)/opkg/$(DOCKER_T)/
	@cd $(PKGDIR)/opkg/ && $(OPKG_DIR)/opkg-build -O $(DOCKER_T)
	@cp $(PKGDIR)/opkg/*.opk $(PKGDIR)/
	@rm -rf $(PKGDIR)/opkg

$(DOCKER_T)-clean: 
	@rm -rf $(DOCKER_SRCDIR)

$(DOCKER_T)-clobber: $(DOCKER_T)-clean
	@rm -rf $(DOCKER_ARCDIR)
	@rm -f .$(DOCKER_T)*

