# Build System For PiDock - docker on PiBox
# This generates the Docker opkg.
# ----------------------------------------------------
all: defaultBB

# These files contain common variables and targets.
include config.mk
include util.mk

# ---------------------------------------------------------------
# Default build
# ---------------------------------------------------------------
defaultBB: .$(WWW_T) 

# ---------------------------------------------------------------
# Cleanup targets - seldom used since they affect all 
# components at once.
# ---------------------------------------------------------------
clean: 
	@for component in $(TARGETS); do \
		echo "Clobbering: $$component"; \
		sudo make --no-print-directory $$component-clean; \
	done

# Careful - this wipes your archive out too!
clobber: 
	@for component in $(TARGETS); do \
		echo "Clobbering: $$component"; \
		sudo make --no-print-directory $$component-clobber; \
	done
	@sudo rm -f Changelog*
	@sudo rm -rf $(ARCDIR) $(BLDDIR) $(PKGDIR)

